<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\RegisterController;
  
Route::get('admission', [RegistrationController::class, 'index']);
Route::post('admission', [RegistrationController::class, 'store']);

Route::get('storage', function () {
    return \Artisan::call('storage:link');
});

Route::group(['namespace' => 'App\Http\Controllers'], function ()
 {
    Route::group(['prefix'=>'admin-control', 'as'=>'admin.'] , function(){

        Route::get('/login','LoginController@index')->name('login');
        Route::post('/login','LoginController@login')->name('login');

      Route::group(['middleware' => 'admin'], function(){
          Route::get('/logout','LoginController@logout')->name('logout');
          Route::get('/','DashboardController@dashboard')->name('dashboard');
          Route::get('editprofile','EditprofileController@index')->name('editprofile');
          Route::get('changepassword','ChangepasswordController@index')->name('changepassword');
          Route::get('sitesetting','SiteController@index')->name('sitesetting');
          
          Route::post('sitesetting','SiteController@store')->name('site.store');
          Route::get('menu_item/delete', 'MenuItemController@destroy')->name('menu_item.destroy');

          // Route::delete('media/delete/{media}', 'MediaController@destroy')->name('media.destroy');
          // Route::get('media/update', 'MediaController@destroy')->name('media.update');
      
          // Route::get('menu', 'SelectmenuController@create')->name('menu.create');
          // Route::get('menu', 'SelectmenuController@index')->name('menu.index');
          // Route::get('menuit', 'MenuController@create')->name('menuit.create');
          // Route::get('menuit', 'MenuController@index')->name('menuit.index');

          Route::resources([
            'menu'      => 'MenuController',
            'menu-item' => 'MenuItemController',
            'media'     =>  'MediaController',
          ]);

          Route::post('menu-item/sort', 'MenuItemController@updateSort')->name('menu-item.update.sort');
        
          Route::get('category/{post_type:slug}', 'CategoryController@index')->name('category.index');
          Route::get('category/{post_type:slug}/create', 'CategoryController@create')->name('category.create');
          Route::get('category/{post_type:slug}/edit/{category:slug}', 'CategoryController@edit')->name('category.edit');
          Route::put('category/{post_type:slug}/update/{category:slug}', 'CategoryController@update')->name('category.update');
          Route::delete('category/{post_type:slug}/delete/{category:slug}', 'CategoryController@destroy')->name('category.destroy');
          Route::post('category/{post_type:slug}/store', 'CategoryController@store')->name('category.store');


          Route::get('{post_type:slug}', 'PostController@index')->name('post.index');
          Route::get('{post_type:slug}/create', 'PostController@create')->name('post.create');
          Route::get('{post_type:slug}/edit/{post:slug}', 'PostController@edit')->name('post.edit');
          Route::put('{post_type:slug}/update/{post:slug}', 'PostController@update')->name('post.update');
          Route::delete('{post_type:slug}/delete/{post:slug}', 'PostController@destroy')->name('post.destroy');
          Route::post('{post_type:slug}/store', 'PostController@store')->name('post.store');
      });
  });

  # Web Routes
  Route::group(['namespace' => 'Web'], function () {
    Route::get('/', 'HomeController@index')->name('Home');
    Route::post('/contact', 'ContactUsController@sendEnquiry')->name('contact.send');
    Route::post('/admission', 'RegistrationController@sendEnquiry')->name('admission.send');
    
    Route::get('/{post:slug}', 'PostController@show')->name('post.show');
  });

  
    
});