function convertToSlug(Text) {
  return Text.toLowerCase()
    .replace(/ /g, '-')
    .replace(/[^\w-]+/g, '');
}

$(function () {
  $(document).on('keyup', '.title', function () {
    var sel = $(this).data('target');

    $(sel).val(convertToSlug($(this).val()));
  });

  $(document).on('click', '.filepicker', function () {
    var target = $(this).data('target');
    $('#target_field').val(target);

    $('#mediaModal').modal('show');
  });

  $(document).on('click', '.media-done-btn', function () {
    if($('[name="media_url"]:checked').length) {
      var target = $('#target_field').val();
  
      $(target).val( $('[name="media_url"]').val() );

      alert('File selected successfully.');

      $('#mediaModal').modal('hide');


    } else {
      alert('Please select at least one file.');
    }
  });

  $(document).on('change', '.post_type', function () {
    $.ajax({
      url: $(this).data('url'),
      method: 'POST',
      data: {
        post_type: $(this).val()
      },
      success: function (res) {
        $('#post_id').html(`<option value>Select Post</option>`);
        $('#category_id').html(`<option value>Select Category</option>`);

        for (var pid in res.posts) {
          $('#post_id').append(`<option value="${pid}">${res.posts[pid]}</option>`);
        }

        for (var cid in res.categories) {
          $('#category_id').append(`<option value="${cid}">${res.categories[cid]}</option>`);
        }
      }
    });
  });

  $(window).on('load', function () {
    if ($('.menu_type').length) $('.menu_type:checked').trigger('click');
  });

  $(document).on('click', '.menu_type', function () {
    var menu_type = $(this).val();

    switch (menu_type) {
      case 'post':
        $('.category, .external').hide();
        $('.post').show();
        break;

      case 'category':
        $('.post, .external').hide();
        $('.category').show();
        break;

      case 'external':
        $('.category, .post').hide();
        $('.external').show();
        break;

      default:
        break;
    }
  });

  $(document).on('change', '.menu', function () {
    var sel = $(this).data('target');
    $.ajax({
      url: $(this).data('url'),
      data: {
        menu_id: $(this).val()
      },
      success: function (res) {
        $(sel).html(`<option value>Select Parent Menu Item</option>`);

        for (var id in res) {
          $(sel).append(`<option value="${id}">${res[id]}</option>`);
        }
      }
    });
  });
});

(function ($) {
  "use strict"; // Start of use strict

  // Toggle the side navigation
  $("#sidebarToggle, #sidebarToggleTop").on('click', function (e) {
    $("body").toggleClass("sidebar-toggled");
    $(".sidebar").toggleClass("toggled");
    if ($(".sidebar").hasClass("toggled")) {
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Close any open menu accordions when window is resized below 768px
  $(window).resize(function () {
    if ($(window).width() < 768) {
      $('.sidebar .collapse').collapse('hide');
    };

    // Toggle the side navigation when window is resized below 480px
    if ($(window).width() < 480 && !$(".sidebar").hasClass("toggled")) {
      $("body").addClass("sidebar-toggled");
      $(".sidebar").addClass("toggled");
      $('.sidebar .collapse').collapse('hide');
    };
  });

  // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
  $('body.fixed-nav .sidebar').on('mousewheel DOMMouseScroll wheel', function (e) {
    if ($(window).width() > 768) {
      var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
      this.scrollTop += (delta < 0 ? 1 : -1) * 30;
      e.preventDefault();
    }
  });

  // Scroll to top button appear
  $(document).on('scroll', function () {
    var scrollDistance = $(this).scrollTop();
    if (scrollDistance > 100) {
      $('.scroll-to-top').fadeIn();
    } else {
      $('.scroll-to-top').fadeOut();
    }
  });

  // Smooth scrolling using jQuery easing
  $(document).on('click', 'a.scroll-to-top', function (e) {
    var $anchor = $(this);
    $('html, body').stop().animate({
      scrollTop: ($($anchor.attr('href')).offset().top)
    }, 1000, 'easeInOutExpo');
    e.preventDefault();
  });

})(jQuery); // End of use strict
