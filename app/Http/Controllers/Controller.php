<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;


use Illuminate\Http\Request;
use App\Models\Media;
use App\Models\Site;
use App\Models\PostType;
use App\Models\MenuItem;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public $_site = null,
           $_post_types = [];
    
    public function __construct( Request $request )
    {
        $domain = $request->getHost();
        $this->_site = $site = Site::where('domain', $domain)->firstOrFail();
        $this->_post_types = $post_types = PostType::where('site_id', $site->id)->get();

        $header_menus = MenuItem::whereHas('menu', function ($q) use ($site) {
            $q->whereNull('menu_item_id')->where('site_id', $site->id)->where('location', 'top-menu');
        })->orderBy('sortby')->get();
        
        $footer_menus = MenuItem::whereHas('menu', function ($q) use ($site) {
            $q->whereNull('menu_item_id')->where('site_id', $site->id)->where('location', 'bottom-menu');
        })->orderBy('sortby')->get();

        $media = Media::where('site_id', $this->_site->id)->get();
        
        \View::share( compact('site', 'post_types', 'header_menus', 'footer_menus', 'media') );
    }
}
