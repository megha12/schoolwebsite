<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\PostType;
use App\Models\PostMeta;
use App\Models\Category;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PostType $post_type)

    {
        $posts = Post::where('post_type', $post_type->slug)->latest()->paginate(10);
        return view('backend.pages.post.index', compact('post_type', 'posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, PostType $post_type)
    {
        $post_fields = $post_type->fields();

        // dd($post_fields->extra_fields);
        $request->flush();
        $parent_categories = Category::with('categories')->where('site_id', $this->_site->id)->whereNull('category_id')->select('title', 'id', 'category_id')->get();
        return view('backend.pages.post.create', compact('post_type', 'post_fields', 'parent_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $post_type)
    {
        
        $post = new Post();
        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->excerpt = $request->excerpt;
        $post->description = $request->description;
        $post->post_type = $post_type;
        $post->site_id = $this->_site->id;
        $post->meta_keywords=$request->meta_keywords;
        $post->meta_title = $request->meta_title;
        $post->meta_description = $request->meta_description;
        $post->template = $request->template;
        
        if ($request->hasfile('image')){
            $post->image = $request->file('image')->store($this->_site->domain . '/imgs/post/', 'public');
        }

        $post->save();

        if(!empty($request->extra_fields)) :
            $post->post_meta()->delete();
            foreach ($request->extra_fields as $meta_name => $meta_value) {
                $post_meta = new PostMeta();
                $post_meta->meta_name = $meta_name;
                $post_meta->meta_value = $meta_value;
                $post_meta->post_id = $post->id;
                $post_meta->save();
            }
        endif;

        if(!empty($request->category)) {
            $post->categories()->sync($request->category);
        }

        return redirect( route('admin.post.index', [$post_type]) );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post  $post)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit( Request $request, $post_type_slug, Post $post)
    {
        $post_type = PostType::where('slug', $post_type_slug)->firstOrFail();
        $post_fields = $post_type->fields();
        $post_arr = $post->toArray();
        $post_arr['category'] = $post->categories()->pluck('categories.id')->toArray();
        $request->replace($post_arr);
        $request->flash();

        $parent_categories = Category::with('categories')->where('site_id', $this->_site->id)->whereNull('category_id')->select('title', 'id', 'category_id')->get();

        return view('backend.pages.post.edit', compact('post', 'post_type', 'post_fields', 'parent_categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $post_type ,$post)
    {
        $post = Post::findOrFail($post);
        $post->post_type=$post_type;
        $post->title = $request->title;
        $post->slug = $request->slug;
        $post->excerpt = $request->excerpt;
        $post->description = $request->description;
        $post->meta_keywords=$request->meta_keywords;
        $post->meta_title = $request->meta_title;
        $post->meta_description = $request->meta_description;
        $post->template = $request->template;
        
        if ($request->hasfile('image')){
            $post->image = $request->file('image')->store($this->_site->domain . '/imgs/post/', 'public');
        }

        $post->save();
        
        $post->categories()->sync( $request->category );

        return redirect( route('admin.post.index', [$post_type]) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy( $post_type, $slug )
    {   
        $post = Post::where('slug',$slug)->where('post_type',$post_type)->firstOrFail();
        $post->delete();
        return redirect( route('admin.post.index',$post_type) );
    }
}