<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $site = $this->_site;
        $request->replace($site->toArray());
        $request->flash();

        return view ('backend.pages.sitesetting');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $site = $this->_site;
        $site->title = $request->title;
        $site->tagline = $request->tagline;
        $site->footer_script = $request->footer_script;
        $site->email = $request->email;
        $site->phone = $request->phone;
        $site->address = $request->address;

        if ($request->hasfile('logo')){
            $site->logo = $request->file('logo')->store($this->_site->domain . '/imgs/logo/', 'public');
        }
        if ($request->hasfile('footer_logo')){
            $site->footer_logo = $request->file('footer_logo')->store($this->_site->domain . '/imgs/footer_logo/', 'public');
        }
        if ($request->hasfile('favicon')){
            $site->favicon = $request->file('favicon')->store($this->_site->domain . '/imgs/favicon/', 'public');
        }

        $site->save();

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function show(Site $site)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function edit(Site $site)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Site $site)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Site  $site
     * @return \Illuminate\Http\Response
     */
    public function destroy(Site $site)
    {
        //
    }
}
