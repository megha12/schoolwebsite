<?php

namespace App\Http\Controllers;

use App\Models\Media;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $media = Media::where('site_id', $this->_site->id)->latest()->paginate(10);
        return view('backend.pages.media.index',compact('media'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->flush();
        $media = Media::where('site_id',$this->_site->id)->pluck('id');

        return view('backend.pages.media.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $media = new Media();
        $media->site_id = $this->_site->id;
        $media->title = $request->title;
        $media->type = $request->type;

    
        
        if ($request->hasfile('file')){
            $media->file = $request->file('file')->store($this->_site->domain . '/imgs/media/', 'public');
        }
        $media->save();
        
        return redirect( route('admin.media.index') );

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function show(Media $media)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function edit(Media $media)
    {
        
        $request->flush();
        $media = Media::where('site_id',$this->_site->id)->pluck('id');

        return view('backend.pages.media.create');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Media $media)
    {
        
        $media->site_id = $this->_site->id;
        $media->title = $request->title;
        
        
        if ($request->hasfile('file')){
            $media->file = $request->file('file')->store($this->_site->domain . '/imgs/post/', 'public');
        }
        $media->save();

        return redirect( route('admin.media.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Media  $media
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $media = Media::findOrFail($id);
        $media->delete();
        return redirect( route('admin.media.index') );
    }
}
