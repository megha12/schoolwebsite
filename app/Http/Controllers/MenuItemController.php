<?php

namespace App\Http\Controllers;

use App\Models\MenuItem;
use App\Models\Post;
use App\Models\PostType;
use App\Models\Menu;
use Illuminate\Http\Request

class MenuItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_post_types = PostType::where('site_id',$this->_site->id)->pluck('name','slug');
        $menus = Menu::with(['menu_items' => function ($q) {
            $q->whereNull('menu_item_id')->orderBy('sortby');
        }])->where('site_id', $this->_site->id)->get();

        // dd($menus);
        return view ('backend.pages.menu_item.index', compact('all_post_types', 'menus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->flush();

        $all_post_types = PostType::where('site_id',$this->_site->id)->pluck('name', 'slug');
        $menus = Menu::where('site_id',$this->_site->id)->pluck('name', 'id');
        $parent = [];
        $posts = [];
        return view ('backend.pages.menu_item.create', compact('all_post_types', 'menus', 'parent', 'posts'));
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $menuItem = new MenuItem();
        $menuItem->menu_id = $request->menu_id;
        $menuItem->menu_item_id = $request->menu_item_id;
        $menuItem->post_type = $request->post_type;
        $menuItem->label = $request->label;
        $menuItem->type = $request->type;
        $menuItem->link = $request->link;
        $menuItem->target = $request->target;
        $menuItem->post_id = $request->post_id;
        $menuItem->category_id = $request->category_id;
        $menuItem->save();

        return redirect( route('admin.menu-item.index') );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MenuItem  $menuItem
     * @return \Illuminate\Http\Response
     */
    public function show(MenuItem $menuItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MenuItem  $menuItem
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, MenuItem $menuItem)
    {
        $request->replace($menuItem->toArray());
        $request->flash();

        $all_post_types = PostType::where('site_id',$this->_site->id)->pluck('name', 'slug');
        $menus = Menu::where('site_id',$this->_site->id)->pluck('name', 'id');
        $parent = [];
        if(!empty($menuItem->menu_id))
        {
            $parent = MenuItem::where('menu_id', $menuItem->menu_id)->pluck('label', 'id');
        }
        $posts = [];
        if(!empty($menuItem->post_type))
        {
            $posts = Post::where('post_type', $menuItem->post_type)->where('site_id', $this->_site->id)->pluck('title', 'id');
        }
        return view ('backend.pages.menu_item.edit', compact('all_post_types', 'menus', 'parent', 'menuItem', 'posts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MenuItem  $menuItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MenuItem $menuItem)
    {
        $menuItem->menu_id = $request->menu_id;
        $menuItem->menu_item_id = $request->menu_item_id;
        $menuItem->post_type = $request->post_type;
        $menuItem->label = $request->label;
        $menuItem->type = $request->type;
        $menuItem->link = $request->link;
        $menuItem->target = $request->target;
        $menuItem->post_id = $request->post_id;
        $menuItem->category_id = $request->category_id;
        $menuItem->save();

        return redirect( route('admin.menu-item.index') );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MenuItem  $menuItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(MenuItem $menuItem)
    {
        $menuItem->delete();
        return redirect( route('admin.menu-item.index') );
    }

    public function updateSort(Request $request)
    {
        foreach ($request->ids as $index => $id) {
            $menuItem = MenuItem::find($id);
            $menuItem->sortby = $index;
            $menuItem->save();
        }

        return response()->json(['msg' => 'order updated']);
    }
}
