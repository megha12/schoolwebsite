<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Post;
use App\Models\Category;

class AjaxController extends Controller
{
    
    public function get_post_and_categories(Request $request)
    {
        $post_type = $request->post_type;

        $posts = Post::where('post_type', $post_type)->where('site_id', $this->_site->id)->pluck('title', 'id');
        $categories = Category::where('type', $post_type)->where('site_id', $this->_site->id)->pluck('title', 'id');

        $re = [
            'posts' => $posts,
            'categories'    => $categories
        ];

        return response()->json($re);
    }   
}
