<?php
namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;

class HomeController extends Controller
{
    public function index()
    {
        $sliders        = Post::where('post_type', 'slider')->latest()->get();
        $about          = Post::where('slug', 'about-us')->first();
        $notices        = Post::where('post_type', 'notice')->latest()->get();
        $events         = Post::where('post_type', 'event')->skip(0)->take(10)->latest()->get();
        $management     = Post::where('post_type', 'management')->latest()->get();
        $news           = Post::where('post_type', 'news')->latest()->get();
        $blogs          = Post::where('post_type', 'post')->latest()->get();
        $testimonials   = Post::where('post_type', 'testimonial')->latest()->get();
        $galleries      = Post::where('post_type', 'Photo-gallary')->skip(0)->take(12)->latest()->get();
        $brands         = Post::where('post_type', 'brands')->latest()->get();

        return view('web.inc.Home', compact('sliders', 'about', 'notices', 'events', 'management', 'news', 'blogs', 'testimonials', 'galleries', 'brands'));
    }
}
