<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Post $post)
    {
        $data = compact('post');
        // dd($post->post_type);
        switch ($post->template) {
            case 'photo-gallery':
                $data['galleries'] = Post::where('post_type', 'Photo-gallary')->paginate(12);
                break;
            
            case 'download':
                $data['downloads'] = Post::where('post_type', 'Download')->paginate(12);
                break;
            
            case 'facility':
                $data['facilities'] = Post::where('post_type', 'facility')->paginate(12);
                break;
            
            case 'tc':
                $data['tcs'] = [];

                if(!empty($request->tc_num) || !empty($request->enroll)) {
                    $posts = Post::query();
                    if(!empty($request->tc_num)) {
                        $posts->whereHas('post_meta', function ($q) use ($request) {
                            $q->where('meta_name', 'tc_number')->where('meta_value', 'LIKE', $request->tc_num.'%');
                        });
                    } else if (!empty($request->enroll)) {
                        $posts->whereHas('post_meta', function ($q) use ($request) {
                            $q->where('meta_name', 'enroll_no')->where('meta_value', 'LIKE', $request->enroll.'%');
                        });
                    }
                    $data['tcs'] = $posts->orderBy('title')->paginate(12);
                }
                break;

            default:
                # code...
                break;
        }
        // dd($data);
        return view('web.inc.' . $post->template, $data);    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        //
    }
}
