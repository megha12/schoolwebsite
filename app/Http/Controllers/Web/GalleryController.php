<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function contains()
    {
        return view('web.inc.gallery');
    }
}
