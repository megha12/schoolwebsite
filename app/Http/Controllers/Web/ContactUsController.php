<?php

namespace App\Http\Controllers\Web;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function relay()
    {
        // print 'nameee';
        return view('web.inc.contact');
    }

    public function index(){
        return view('web.inc.contact');
    }

    public function sendEnquiry(Request $request)
    {
        
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'g-recaptcha-response' => 'required|recaptchav3:register,0.5'
        ]);

        $details = $request->except('_token');
        $details['subject'] = 'Contact enquiry from ' . $this->_site->title;
        $details['site'] = $this->_site;
        $details['msg'] = $request->message;

        // $details = array_merge($details, );

        // dd($details);
       
        \Mail::to($this->_site->email)
            ->cc('sudarshandubaga@gmail.com')
            ->send(new \App\Mail\ContactMail($details));

        return redirect()->back()->with('success', 'Your details has been sent. We will contact you shortly.');
    }
}
