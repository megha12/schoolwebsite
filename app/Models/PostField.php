<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostField extends Model
{
    use HasFactory;

    protected $casts = [
        'extra_fields' => 'array'
    ];
}