<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;

    protected $casts = [
        'location' => 'array',
    ];

    public function menu_items()
    {
        return $this->hasMany(MenuItem::class)->orderBy('sortby');
    }
}
