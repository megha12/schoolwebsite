<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    public function categories()
    {
        return $this->belongsToMany(Category::class, 'post_categories');
    }

    public function post_meta()
    {
        return $this->hasMany(PostMeta::class);
    }

    public function getMetaValue($meta_name)
    {
        $field = PostMeta::where('post_id', $this->id)->where('meta_name', $meta_name)->first();
        return !empty($field->meta_value) ? $field->meta_value : "";
    }
}
