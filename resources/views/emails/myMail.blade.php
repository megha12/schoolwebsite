<!DOCTYPE html>
<html>
<head>
    <title>ItsolutionStuff.com</title>
</head>
<body>
    <div>
        <!-- Header -->
        <header style="background: #ebebeb; text-align: center; padding: 40px 0;">
            <img src="{{ url('storage/' . $site->logo) }}" alt="{{ $site->title }}" style="height: 100px;">
        </header>
        <!-- ./ Header -->
        <div>
            Dear Admin,
        </div>
        <p>You have received an contact enquiry on your website with following details:</p>
    
        <table style="width: 100%;">
            @if(!empty($name))    
            <tr>
                <th align="left">Name</th>
                <td>{{ $name }}</td>
            </tr>
            @endif
            @if(!empty($email))
            <tr>
                <th align="left">Email</th>
                <td>{{ $email }}</td>
            </tr>
            @endif
            @if(!empty($how_know))
            <tr>
                <th align="left">Source</th>
                <td>{{ $how_know }}</td>
            </tr>
            @endif
            @if(!empty($msg))
            <tr>
                <th align="left">Message</th>
                <td>{{ $msg }}</td>
            </tr>
            @endif
        </table>
       
        <p>
            Thanks &amp; Regards<br>
            {{ $site->title }}
        </p>
    </div>
</body>
</html>