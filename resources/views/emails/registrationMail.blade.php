<!DOCTYPE html>
<html>
<head>
    <title>ItsolutionStuff.com</title>
</head>
<body>
    <div>
        <!-- Header -->
        <header style="background: #ebebeb; text-align: center; padding: 40px 0;">
            <img src="{{ url('storage/' . $site->logo) }}" alt="{{ $site->title }}" style="height: 100px;">
        </header>
        <!-- ./ Header -->
        <div>
            Dear Admin,
        </div>
        <p>You have received an Registration enquiry on your website with following details:</p>
    
        <table style="width: 100%;">
            @if(!empty($year))    
            <tr>
                <th align="left">Admission For Academic Year</th>
                <td>{{ $year }}</td>
            </tr>
            @endif
            @if(!empty($class))    
            <tr>
                <th align="left">Admission Sought to</th>
                <td>{{ $class }}</td>
            </tr>
            @endif
            @if(!empty($name))
            <tr>
                <th align="left">Student's Name</th>
                <td>{{ $name }}</td>
            </tr>
            @endif
            @if(!empty($father_name))
            <tr>
                <th align="left">Father's Name</th>
                <td>{{ $father_name }}</td>
            </tr>
            @endif
            @if(!empty($father_occupation))
            <tr>
                <th align="left">Occupation</th>
                <td>{{ $father_occupation }}</td>
            </tr>
            @endif
            
            @if(!empty($father_phone))
            <tr>
                <th align="left">Father's Mobile No.</th>
                <td>{{ $father_phone }}</td>
            </tr>
            @endif
            @if(!empty($father_email))
            <tr>
                <th align="left">Email Address</th>
                <td>{{ $father_email }}</td>
            </tr>
            @endif
            @if(!empty($mother_name))
            <tr>
                <th align="left">Mother's Name</th>
                <td>{{ $mother_name }}</td>
            </tr>
            @endif
            @if(!empty($mother_occupation))
            <tr>
                <th align="left">Occupation</th>
                <td>{{ $mother_occupation }}</td>
            </tr>
            @endif           
            @if(!empty($mother_phone))
            <tr>
                <th align="left">Mother's Mobile No.</th>
                <td>{{ $mother_phone }}</td>
            </tr>
            @endif
            @if(!empty($mother_email))
            <tr>
                <th align="left">Email Adress</th>
                <td>{{ $mother_email }}</td>
            </tr>
            @endif          
        </table>
       
        <p>
            Thanks &amp; Regards<br>
            {{ $site->title }}
        </p>
    </div>
</body>
</html>