<div class="mb-3">
                    {{ Form::label('title') }}
                    {{ Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Enter Title']) }}
                </div>
                <div class="mb-3">
                    {{ Form::label('slug') }}
                    {{ Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Enter SEO URL']) }}
                </div>
                <div class="mb-3">
                    {{ Form::label('excerpt') }}
                    {{ Form::textarea('excerpt', null, ['class' => 'form-control', 'placeholder' => 'Enter Short Description']) }}
                </div>
                <div class="mb-3">
                    {{ Form::label('description') }}
                    {{ Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Enter Description']) }}
                </div>
                