@extends('backend.layouts.inner')
@section('site_title','Add Page')
@section('content')


<main>
 <div class="container-fluid px-4">
   <h1 class="mt-4">Dashboard</h1>
        <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard')  }}">Basic Information</a>
                </li>
                <li class="breadcrumb-item active">Add Pages</li>
         </ol>


         <div class="card mb-4">
            <div class="card-header">
                <i class="fas fa-table mr-1"></i>
                Basic Information
            </div>
            <div class="card-body">
                {{ Form::open(['url' => route('admin.page.store'), 'method' => 'post']) }}
                @include('backend.pages.page.form')
                <div>
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</main>
@endsection