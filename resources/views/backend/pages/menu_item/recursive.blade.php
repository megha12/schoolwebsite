<ul class="ul-sortable">
    @foreach($mitems as $mitem2)
        <li id="item-{{ $mitem2->id }}" data-id="{{ $mitem2->id }}">
            <div class="menu-item-list">
            <strong>{{ $mitem2->label }}</strong><br>
            <div class="d-flex align-items-center">
            {{ $mitem2->type }}
             <!-- <a href=""> <i class="fas fa-ellipsis-v"></i> </a> -->
                <div class="dropdown">
                    <button class="btn btn-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v"></i>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ route('admin.menu-item.edit', [$mitem2->id]) }}">Edit</a>
                        {{ Form::open(['url' => route('admin.menu-item.destroy', [$mitem2->id]), 'method' => 'delete']) }}
                        <button class="dropdown-item">Delete</button>
                        {{ Form::close() }}
                    </div>
                </div>
             </div>
             </div>
            @if(!$mitem2->menu_item->isEmpty())
                @include('backend.pages.menu_item.recursive', ['mitems' => $mitem2->menu_item])
            @endif
        </li>
    @endforeach
</ul>