@extends('backend.layouts.inner')
@section('site_title','Add menu items')
@section('content')


<div class="container-fluid">
    {{ Form::open(['url' => route('admin.menu-item.store')]) }}
    <div>
        @include('backend.pages.menu_item.form')
        <div class="mb-3">
            <button class="btn ol btn-outline-primary btn-block">ADD TO MENU</button>
        </div>
    </div>
    {{ Form::close() }}

</div>


@endsection