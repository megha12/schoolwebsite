<div>
    <div class="form-check form-check-inline">
        {{ Form::radio('type', 'post', 1, ['class' => 'form-check-input menu_type', 'id' => 'type_post']) }}
        {{ Form::label('type_post', 'Post', ['class' => 'form-check-label']) }}
    </div>
    <div class="form-check form-check-inline">
        {{ Form::radio('type', 'category', null, ['class' => 'form-check-input menu_type', 'id' => 'type_category']) }}
        {{ Form::label('type_category', 'Category', ['class' => 'form-check-label']) }}
    </div>
    <div class="form-check form-check-inline">
        {{ Form::radio('type', 'external', null, ['class' => 'form-check-input menu_type', 'id' => 'type_external']) }}
        {{ Form::label('type_external', 'Other', ['class' => 'form-check-label']) }}
    </div>
</div>
<div class="mb-3">
    {{ Form::label('menu_id') }}
    {{ Form::select('menu_id', $menus, null, ['class' => 'form-control menu', 'placeholder' => 'Select Menu', 'data-url' => route('ajax.menu-item.index'), 'data-target' => '#menu_item_id','required' => 'required']) }}
</div>
        
<div class="mb-3">
    {{ Form::label('menu_item_id', 'Parent Menu Item') }}
    {{ Form::select('menu_item_id', $parent, null, ['class' => 'form-control', 'placeholder' => 'Select Parent Menu Item']) }}
</div>  
<div class="mb-3 category post">
    {{ Form::label('post_type') }}
    {{ Form::select('post_type', $all_post_types, null, ['class' => 'form-control post_type', 'placeholder' => 'Select Post Type', 'data-url' => route('ajax.post_category')]) }}
</div>
<div class="mb-3 category" style="display: none">
    {{ Form::label('category_id', 'Category') }}
    {{ Form::select('category_id', [], null, ['class' => 'form-control', 'placeholder' => 'Select Category']) }}
</div>
<div class="mb-3 post">
    {{ Form::label('post_id', 'Post') }}
    {{ Form::select('post_id', $posts, null, ['class' => 'form-control', 'placeholder' => 'Select Post']) }}
</div>
<div class="mb-3">
    {{ Form::label('label') }}
    {{ Form::text('label', null, ['class' => 'form-control', 'placeholder' => 'Enter Label']) }}
</div>
<div class="mb-3 external" style="display: none">
    {{ Form::label('link') }}
    {{ Form::text('link', null, ['class' => 'form-control', 'placeholder' => 'Enter Link']) }}
</div>
<div class="mb-3">
    {{ Form::label('target') }}
    {{ Form::select('target', ['_self' => 'Current Tab', '_blank' => 'New Tab'], null, ['class' => 'form-control']) }}
</div>
<div class="mb-3">
    {{ Form::label('follow') }}
    {{ Form::select('follow', ['dofollow' => 'Do Follow', 'nofollow' => 'No Follow'], null, ['class' => 'form-control']) }}
</div>