@extends('backend.layouts.inner')
@section('site_title','Edit menu items')
@section('content')


<div class="container-fluid">
    {{ Form::open(['url' => route('admin.menu-item.update', [$menuItem->id]), 'method' => 'PUT']) }}
        <div>
            @include('backend.pages.menu_item.form')
            <div class="mb-3">
                <button class="btn ol btn-outline-primary btn-block">Update</button>
            </div>
        </div>
    {{ Form::close() }}

</div> 
    

@endsection