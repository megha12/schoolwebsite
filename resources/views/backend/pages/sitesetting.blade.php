 @extends('backend.layouts.inner')

@section('site_title', 'Site Setting | ')

@section('content')
    <div class="container-fluid py-4">
        {{ Form::open(['url' => route('admin.site.store'), 'files' => true]) }}
       <div>
                <div class="mb-3">
                {{ Form::label('title', 'Site Title') }}
                {{ Form::text('title', null, ['class' => 'form-control title', 'placeholder' => 'Enter Title', 'data-target' => '#slug']) }}
            </div>
            <div class="mb-3">
                {{ Form::label('tagline') }}
                {{ Form::text('tagline', null, ['class' => 'form-control', 'placeholder' => 'Enter tagline']) }}
            </div>
            <div class="mb-3">
                {{ Form::label('logo') }}
                {{ Form::file('logo', ['class' => 'form-control'])}}
            </div>
            <div class="mb-3">
                {{ Form::label('footer_logo') }}
                {{ Form::file('footer_logo', ['class' => 'form-control'] )}}
            </div>
        </div>
            <div class=" mb-3 py-4">
                {{ Form::label('favicon') }}
                {{ Form::file('favicon', ['class' => 'form-control']) }}
            </div>
            <div class=" mb-3  py-4">
                {{ Form::label('footer_script') }}
                {{ Form::textarea('footer_script', null, ['class' => 'form-control', 'placeholder' => 'Enter Description']) }}
            </div>
            <div class="mb-3  py-4">
                {{ Form::label('email') }}
                {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter email']) }}
            </div>
            <div class="mb-3  py-4">
                {{ Form::label('phone') }}
                {{ Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Enter phone no.']) }}
            </div>
            <div class="mb-3  py-4">
                {{ Form::label('address') }}
                {{ Form::textarea('address', null, ['class' => 'form-control', 'placeholder' => 'Enter your address']) }}
            </div>
            <div class="text-center">
            <button type="submit" class="btn btn-primary btn-lg">Save</button>
            </div>
        </div> 
        {{ Form::close() }}   
    </div>
@endsection