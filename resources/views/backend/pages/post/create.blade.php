@extends('backend.layouts.inner')
@section('site_title','Add page')
@section('content')

<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Add {{$post_type->name}}</h1>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard')}}">Dashboard</a>
                </li>
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.post.index', $post_type->slug)}}">{{ $post_type->name }}</a>
                </li>
                <li class="breadcrumb-item active">Add {{$post_type->name}}</li>
            </ol>
        </div>
        <div class="card-body">
            {{ Form::open(['url' => route('admin.post.store' ,$post_type->slug),  'files' => true, 'method' => 'post']) }}
            @include('backend.pages.post.form')
            <div class="mt-3">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="reset" class="btn btn-secondary">Reset</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>

</div>
@endsection