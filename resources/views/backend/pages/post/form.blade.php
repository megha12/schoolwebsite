<div class="row">
    <div class="col-lg-8">
        @if(!$post_fields || !empty($post_fields->title))
        @php
        $label = !empty($post_fields->title) ? $post_fields->title : 'Title';
        @endphp
        <div class="mb-3">
            {{ Form::label('title', $label) }}
            {{ Form::text('title', null, ['class' => 'form-control title', 'placeholder' => 'Enter ' . $label, 'data-target' => '#slug']) }}
        </div>
        @endif
        <div class="mb-3">
            {{ Form::label('slug') }}
            {{ Form::text('slug', null, ['class' => 'form-control', 'placeholder' => 'Enter Slug']) }}
        </div>
        @if(!$post_fields || !empty($post_fields->excerpt))
        @php
        $label = !empty($post_fields->excerpt) ? $post_fields->excerpt : 'Short Description';
        @endphp
        <div class="mb-3">
            {{ Form::label('excerpt', $label) }}
            {{ Form::textarea('excerpt', null, ['class' => 'form-control', 'placeholder' => 'Enter ' . $label]) }}
        </div>
        @endif

        @if(!$post_fields || !empty($post_fields->description))
        @php
        $label = !empty($post_fields->description) ? $post_fields->description : 'Description';
        @endphp
        <div class="mb-3">
            {{ Form::label('description', $label) }}
            {{ Form::textarea('description', null, ['class' => 'form-control myEditor', 'placeholder' => 'Enter ' . $label]) }}
        </div>
        @endif


        @if(!empty($post_fields->extra_fields))
        @foreach ($post_fields->extra_fields as $pf)
        <div class="mb-3">
            {{ Form::label('extra_fields['.$pf['meta_name'].']', $pf['label']) }}

            @if($pf['type'] === 'file')
            {{ Form::hidden('extra_fields['.$pf['meta_name'].']', null, ['id' => 'file_' . $pf['meta_name']]) }}
            <!-- Button trigger modal -->
            <div>
                <button type="button" class="btn btn-primary filepicker" data-target="#file_{{ $pf['meta_name'] }}">
                    Choose File
                </button>
            </div>
            @else
            {{ Form::text('extra_fields['.$pf['meta_name'].']', null, ['class' => 'form-control', 'placeholder' => 'Enter ' . $pf['label']]) }}
            @endif
        </div>
        @endforeach
        @endif
    </div>

    <div class="col-lg-4">

        @if(!$post_fields || !empty($post_fields->image))

        @php
        $label = !empty($post_fields->image) ? $post_fields->image : 'Featured Image';
        @endphp
        <div class="mb-3">
            {{ Form::label('image', $label) }}
            @if(!empty($post->image))
            <label for="image" class="card mb-3" style="width:10rem;">
                <img src="{{url('storage/'.$post->image)}}" alt=".." class="card-img-top" name="image">
            </label>
            @endif
            <label class="btn btn-block btn-primary">
                {{Form::file('image',['style'=>'display:none;']) }}
                Choose Image
            </label>
        </div>
        @endif

        @if($post_type->is_category == 'Y')

        <div class="main_category">
            <h1 class="myhedding">CATEGORY</h1>
            <ul>
                @foreach($parent_categories as $pc)
                <li>
                    <!-- <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                MEN'S WEAR<b class="caret"></b>
                </a> -->
                    <label>
                        {{ Form::checkbox('category[]', $pc->id) }}
                        {{ $pc->title }}
                    </label>
                    <ul>
                        @foreach($pc->categories as $subc)
                        <li><label class="checkbox">
                                {{ Form::checkbox('category[]', $subc->id) }}
                                {{ $subc->title }}
                            </label>
                        </li>
                        @endforeach
                    </ul>
                </li>
                @endforeach
            </ul>
        </div>
        @endif

        @if(!$post_fields || !empty($post_fields->meta_title))

        @php
        $label = !empty($post_fields->title) ? $post_fields->title : 'Title';
        @endphp
        <div class="mb-3">
            {{ Form::label('meta_title', $label) }}
            {{ Form::text('meta_title', null, ['class' => 'form-control', 'placeholder' => 'Enter ' . $label]) }}
        </div>
        @endif

        @if(!$post_fields || !empty($post_fields->meta_keywords))
        @php
        $label = !empty($post_fields->meta_keywords) ? $post_fields->meta_keywords : 'Meta Keywords';
        @endphp
        <div class="mb-3">
            {{ Form::label('meta_keywords', $label) }}
            {{ Form::text('meta_keywords', null, ['class' => 'form-control', 'placeholder' => 'Enter ' . $label]) }}
        </div>
        @endif

        @if(!$post_fields || !empty($post_fields->meta_description))
        @php
        $label = !empty($post_fields->meta_description) ? $post_fields->meta_description : 'Meta Description';
        @endphp
        <div class="mb-3">
            {{ Form::label('meta_description', $label) }}
            {{ Form::textarea('meta_description', null, ['class' => 'form-control', 'placeholder' => 'Enter ' . $label]) }}
        </div>
        @endif

        @if(!$post_fields || !empty($post_fields->template))
        @php
        $label = !empty($post_fields->template) ? $post_fields->template : 'Template';
        @endphp
        <div class="mb-3">
            {{ Form::label('template', $label) }}
            {{ Form::select('template', ['page' => 'Page', 'photo-gallery' => 'Photo Gallery', 'home' => 'Home', 'contact' => 'Contact', 'download' => 'Download', 'facility' => 'Facility', 'admission' => 'Online Admission', 'tc' => 'TC'], 'page', ['class' => 'form-control', 'placeholder' => 'Select ' . $label]) }}

            
        </div>
        @endif

    </div>