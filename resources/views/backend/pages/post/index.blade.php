@extends('backend.layouts.inner')
@section('site_title','View Page')
@section('content')
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Tables</h1>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                                <ol class="breadcrumb mb-4">
                                    <li class="breadcrumb-item">
                                        <a href="{{ route('admin.dashboard')}}">Dasgboard</a>
                                    </li>
                                    <li class="breadcrumb-item active">View {{$post_type->name}}</li>
                                </ol>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>Title</th>
                                        <th>Short-Discription</th>
                                        <th>Image</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                  @foreach($posts as $i => $p)
                 <tr>
                    <td>{{ $i + $posts->firstItem() }}</td>
                    <td>{{ $p->title }}</td>
                    <td>{{ $p->excerpt }}</td>
                  
                    <td>
                        <img src="{{ url('storage/'.$p->image) }}" alt="" style="width: 50px; height: 50px; object-fit: contain">
                    </td>
                    <td>
                        <div class="dropdown">
                            <button class="btn btn-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="{{ route('admin.post.edit', [$post_type->slug, $p->slug]) }}">Edit</a>
                                {{ Form::open(['url' => route('admin.post.destroy', [$post_type->slug, $p->slug]), 'method' => 'delete']) }}
                                <button class="dropdown-item">Delete</button>
                                {{ Form::close() }}
                            </div>
                        </div>                 
                    </td>
                </tr>
                @endforeach
                </tbody>
                </table>
             {{ $posts->links("pagination::bootstrap-4") }}
                        </div>
                    </div>
                    </div>
                </div>
@endsection