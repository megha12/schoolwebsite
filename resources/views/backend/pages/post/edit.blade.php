@extends('backend.layouts.inner')
@section('site_title','Add post')
@section('content')


<div class="container-fluid">
    <h1 class="h3 mb-2 text-gray-800">Tables</h1>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard')}}">Database Table</a>
                </li>
                <li class="breadcrumb-item active">Add Post</li>
            </ol>
        </div>
        <div class="card-body">

            {{ Form::open(['url' => route('admin.post.update', [$post_type->slug, $post->id]), 'files' => true, 'method' => 'PUT'] )}}
            @include('backend.pages.post.form')

            <div class="mt-3">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="reset" class="btn btn-secondary">Reset</button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

@endsection