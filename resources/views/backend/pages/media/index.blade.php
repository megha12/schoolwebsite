@extends('backend.layouts.inner')
@section('site_title','View Media')
@section('content')
<div class="card">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    Dashboard
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            
                                <tr>
                                    <th>S. No.</th>
                                    <th>Name</th>
                                    <th>File link</th>
                                    <th>Action</th>
                                </tr>
                                <tbody>
                                    @foreach($media as $i => $m)
                                    <tr>
                                        <td>{{ $i + $media->firstItem() }}</td>
                                        <td>{{ $m->title }}</td>
                                            <td>
                                                <a href="{{ url('storage/'.$m->file) }}" target="_blank">View / Download</a>
                                            </td>
                                        <td>
                                        <div class="dropdown">
                                    <button class="btn btn-link" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="fas fa-ellipsis-v"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <!-- <a class="dropdown-item" href="{{ route('admin.media.edit', [$m->id]) }}">Copy</a> -->
                                        {{ Form::open(['url' => route('admin.media.destroy', [$m->id]), 'method' => 'delete']) }}
                                        <button class="dropdown-item">Delete</button>
                                        {{ Form::close() }}
                                    </div>
                                        </td>
                                    </tr>
                                    @endforeach
                           </tbody>
                        </table> 
                        {{ $media->links("pagination::bootstrap-4") }}
                    </div>  
                </div>  
            </div>
@endsection