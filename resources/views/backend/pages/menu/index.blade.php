@extends('backend.layouts.inner')
@section('site_title','View menu items')
@section('content')
           <div class="card">
                <div class="card-header">
                    <i class="fas fa-table mr-1"></i>
                    DataTable Example
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            
                                <tr>
                                    <th>S. No.</th>
                                    <th>Name</th>
                                    <th>Location</th>
                                </tr>
                                <tbody>
                                    @foreach($menus as $i => $m)
                                    <tr>
                                        <td>{{ $i + $menus->firstItem() }}</td>
                                        <td>{{ $m->name }}</td>
                                        <td>{{ $m->location }}</td>
                                    </tr>
                                    @endforeach
                           </tbody>
                        </table> 
                    </div>  
                </div>  
            </div>
@endsection