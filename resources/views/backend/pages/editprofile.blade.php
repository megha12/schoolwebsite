@extends('backend.layouts.inner')
@section('content')
<div class="container rounded bg-white align-items-center mb-5">
    <!-- <div class="row"> -->
        <div class="col-lg-3"></div>
        <div class="col-lg-12 border-right ">
            
            <div class="d-flex flex-column align-items-center text-center p-3 "><img class="rounded-circle mt-1" src="{{url('assets\images\menu\banner-1.jpg')}}"><span class="font-weight-bold">Amelly</span><span class="text-black-50">amelly12@bbb.com</span><span> </span></div>
        
    
            <div class="p-3 ">
                <div class="d-flex justify-content-between align-items-center mb-3">
                    <h4 class="text-right">Profile Settings</h4>
                </div>
                <div class="row mt-2">
                    <div class="col-md-6"><label class="labels">Name</label><input type="text" class="form-control" placeholder="first name" value=""></div>
                    <div class="col-md-6"><label class="labels">Surname</label><input type="text" class="form-control" value="" placeholder="surname"></div>
                </div>
                <div class="row mt-3">
                    <div class="col-md-12"><label class="labels">PhoneNumber</label><input type="text" class="form-control" placeholder="enter phone number" value=""></div>
                    <div class="col-md-12"><label class="labels">BIO</label><input type="text" class="form-control" placeholder="enter Bio" value=""></div>
                </div>
                
                <div class="mt-5 text-center"><button class="btn btn-primary profile-button" type="button">Save Profile</button></div>
            </div>
        
</div>
    </div>
</div>
</div>
</div>
@endsection