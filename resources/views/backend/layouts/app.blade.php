<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('site_title') {{ $site->title }}</title>

    <!-- Custom fonts for this template-->
    <link href="{{url('admin/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link
        href="{{url('https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i')}}"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{url('admin/css/sb-admin-2.min.css')}}" rel="stylesheet">

</head>

<body id="page-top">

@yield('main_content')

 <!-- Bootstrap core JavaScript-->
    <script src="{{url('vendor/jquery/jquery.min.js')}}" ></script>
    <script src="{{url('vendor/bootstrap/js/bootstrap.bundle.min.js')}}" ></script>

    <!-- Core plugin JavaScript-->
    <script src="{{url('vendor/jquery-easing/jquery.easing.min.js')}}" ></script>

    <!-- Custom scripts for all pages-->
    <script src="{{url('admin/js/sb-admin-2.js')}}" ></script>

    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        tinymce.init({
          selector: '.myEditor',
          branding: false,
          menubar: false,
          plugins: 'lists link code',
          toolbar: 'bold italic underline strikethrough | h1 h2 h3 | alignleft aligncenter alignright alignjustify | numlist bullist | link | code'
        });
        
        $(function () {
            $(document).on('submit', '#media-form', function (e) {
                e.preventDefault();

                var formData;

                $.ajax({
                    url: $(this).attr('action'),
                    type: 'POST',
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function (res) {
                        $('#mediaTab a[href="#select-file"]').tab('show');

                        var imageUrl = 'https://cdn-icons-png.flaticon.com/512/3997/3997608.png';
                        if(res.type == "image") {
                            imageUrl = "{{ url('storage') }}/" + res.file;
                        }

                        $('#media-files').append(`
                            <label class="media-box text-center">
                                <div>
                                    <input type="radio" name="media_url" value="${res.file}">
                                    <img src="${imageUrl}" alt="">
                                </div>
                                <div style="font-size: 10px;">
                                    ${res.title}
                                </div>
                            </label>
                        `);

                        $('#media-files .media-box:last-child').find('input[type=radio]').prop('checked', 'checked');
                    },
                    error: function (err) {
                        console.log('File upload error: ', err);
                    }
                });
            });
        });
    </script>

    @yield('extra_scripts')

</body>

</html>