<ul class="thired-level">
    @foreach($mitems as $mitem2)
        @php
            $link = '';
            switch($mitem2->type) {
                
                case 'post':
                    $link = route('post.show', $mitem2->post->slug);
                    break;
                    
                case 'category':
                    $link = route('post.index', ['category' => @$mitem2->category->slug]);
                    break;

                case 'external':
                    
                default:
                    $link = $mitem2->link;
                    break;
            }
        @endphp
        <li{{ !$mitem2->menu_item->isEmpty() ? ' class=has-child-menu' : '' }}>
            <a href="{{ $link }}" target="{{ $mitem2->target }}">{{ $mitem2->label }}</a>
            @if(!$mitem2->menu_item->isEmpty())
                @include('web.layouts.recursivemenu', ['mitems' => $mitem2->menu_item])
            @endif
        </li>
    @endforeach
</ul>