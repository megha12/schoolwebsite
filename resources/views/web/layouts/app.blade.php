<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ $site->title }}</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('storage/' . $site->favicon) }}">
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="css/normalize.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="css/main.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="css/animate.min.css">
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!-- Owl Caousel CSS -->
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.carousel.min.css">
    <link rel="stylesheet" href="vendor/OwlCarousel/owl.theme.default.min.css">
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="css/meanmenu.min.css">
    <!-- nivo slider CSS -->
    <link rel="stylesheet" href="vendor/slider/css/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="vendor/slider/css/preview.css" type="text/css" media="screen" />
    <!-- Datetime Picker Style CSS -->
    <link rel="stylesheet" href="css/jquery.datetimepicker.css">
    <!-- Magic popup CSS -->
    <link rel="stylesheet" href="css/magnific-popup.css">
    <!-- Switch Style CSS -->
    <link rel="stylesheet" href="css/hover-min.css">
    <!-- ReImageGrid CSS -->
    <link rel="stylesheet" href="css/reImageGrid.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="style.css">
    <!-- Modernizr Js -->
    <script src="js/modernizr-2.8.3.min.js"></script>
</head>

<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- Add your site or application content here -->
    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <!-- Main Body Area Start Here -->
    <div id="wrapper">
        <!-- Header Area Start Here -->
        <header>
            <div id="header2" class="header2-area">
                <div class="header-top-area">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="header-top-left">
                                    <ul>
                                        <li><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:{{ $site->phone }}">{{ $site->phone }} </a></li>
                                        <li><i class="fa fa-envelope" aria-hidden="true"></i><a href="mailto: {{ $site->email }}">{{ $site->email }}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                                <div class="header-top-right">
                                    <ul>
                                        <li>
                                            <!-- <a class="login-btn-area" href="#" id="login-button"><i class="fa fa-lock"
                                                    aria-hidden="true"></i> Login</a>
                                            <div class="login-form" id="login-form" style="display: none;">
                                                <div class="title-default-left-bold">Login</div>
                                                <form>
                                                    <label>Username or email address *</label>
                                                    <input type="text" placeholder="Name or E-mail" />
                                                    <label>Password *</label>
                                                    <input type="password" placeholder="Password" />
                                                    <label class="check">Lost your password?</label>
                                                    <span><input type="checkbox" name="remember" />Remember Me</span>
                                                    <button class="default-big-btn" type="submit" value="Login">ERP
                                                        Login</button>
                                                    <button class="default-big-btn form-cancel" type="submit"
                                                        value="">Cancel</button>
                                                </form>
                                            </div> -->
                                            <a href="#" target="_blank">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                            <a href="#" target="_blank">
                                                <i class="fa fa-instagram"></i>
                                            </a>
                                            <a href="#" target="_blank">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li>

                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="main-menu-area bg-textPrimary" id="sticker">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-2 col-lg-2 col-md-3">
                                <div class="logo-area">
                                    <a href="{{ route('Home') }}">
                                        <img class="img-responsive" src="{{ url('storage/' . $site->logo) }}" alt="Logo of {{ $site->title }}" title="Logo of {{ $site->title }}">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-10 col-lg-10 col-md-9">
                                <nav id="desktop-nav">
                                    <ul>
                                        @foreach($header_menus as $mitem)
                                        @php
                                        $link = '';
                                        switch($mitem->type) {
                                            
                                            case 'post':
                                                $link = route('post.show', $mitem->post->slug);
                                                break;
                                                
                                            case 'category':
                                                $link = route('post.index', ['category' => @$mitem->category->slug]);
                                                break;

                                            case 'external':
                                                
                                            default:
                                                $link = $mitem->link;
                                                break;
                                        }
                                        @endphp
                                        <li>      
                                            <a href="{{ $link }}" target="{{ $mitem->target }}">{{ $mitem->label }}</a>
                                            @if(!$mitem->menu_item->isEmpty())
                                            @include('web.layouts.recursivemenu', ['mitems' => $mitem->menu_item])
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>

                                </nav>
                            </div>
                            <!-- <div class="col-xl-1 col-lg-1 d-none d-lg-block">
                                <div class="header-search">
                                    <form>
                                        <input type="text" class="search-form" placeholder="Search...." required="">
                                        <a href="#" class="search-button" id="search-button"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    </form>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu Area Start -->
            <div class="mobile-menu-area">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mobile-menu">
                                <nav id="dropdown">
                                    <ul>
                                        @foreach($header_menus as $mitem)
                                        @php
                                        $link = '';
                                        switch($mitem->type) {
                                            
                                            case 'post':
                                                $link = route('post.show', $mitem->post->slug);
                                                break;
                                                
                                            case 'category':
                                                $link = route('post.index', ['category' => @$mitem->category->slug]);
                                                break;

                                            case 'external':
                                                
                                            default:
                                                $link = $mitem->link;
                                                break;
                                        }
                                        @endphp
                                        <li>      
                                            <a href="{{ $link }}">{{ $mitem->label }}</a>
                                            @if(!$mitem->menu_item->isEmpty())
                                            @include('web.layouts.recursivemenu', ['mitems' => $mitem->menu_item])
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Mobile Menu Area End -->
        </header>
        <!-- Header Area End Here -->
        @yield('main_section')
        <!-- Footer Area Start Here -->
        <footer>
            <div class="footer-area-top">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="footer-box">
                                <div class="ilogo">
                                    <a href="{{ route('Home') }}">
                                        <img class="img-responsive" src="{{ url('storage/' . $site->footer_logo) }}" alt="Logo of {{ $site->title }}" title="Logo of {{ $site->title }}">
                                    </a>
                                </div>
                                <div class="footer-about">
                                    <p>Praesent vel rutrum purus. Nam vel dui eu sus duis dignissim dignissim.
                                        Suspenetey disse at ros tecongueconsequat.Fusce sit amet rna feugiat.</p>
                                </div>
                                <ul class="footer-social">

                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12 ms-auto">
                            <div class="footer-box">
                                <h3>Useful Links</h3>
                                <ul class="featured-links">
                                    <li>
                                        <ul>
                                            @foreach($footer_menus as $mitem)
                                            @php
                                            $link = '';
                                            switch($mitem->type) {
                                                
                                                case 'post':
                                                    $link = route('post.show', $mitem->post->slug);
                                                    break;
                                                    
                                                case 'category':
                                                    $link = route('post.index', ['category' => @$mitem->category->slug]);
                                                    break;
    
                                                case 'external':
                                                    
                                                default:
                                                    $link = $mitem->link;
                                                    break;
                                            }
                                            @endphp
                                            <li>      
                                                <a href="{{ $link }}">{{ $mitem->label }}</a>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="footer-box">
                                <h3>Information</h3>
                                <ul class="corporate-address">
                                    <li><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:{{ $site->phone }}">
                                            {{ $site->phone }} </a></li>
                                    <li><i class="fa fa-envelope-o" aria-hidden="true"></i>
                                       <a href="mailto:{{  $site->email }}\">{{  $site->email }}</a> 
                                    </li>
                                </ul>

                            </div>
                        </div>
                        <!-- <div class="col-xl-3 col-lg-3 col-md-6 col-sm-12">
                            <div class="footer-box">
                                <h3>Gallary</h3>
                                <ul class="flickr-photos">
                                    <li>
                                        <a href="#"><img class="img-responsive" src="img/footer/1.jpg" alt="flickr"></a>
                                    </li>
                                    <li>
                                        <a href="#"><img class="img-responsive" src="img/footer/2.jpg" alt="flickr"></a>
                                    </li>
                                    <li>
                                        <a href="#"><img class="img-responsive" src="img/footer/3.jpg" alt="flickr"></a>
                                    </li>
                                    <li>
                                        <a href="#"><img class="img-responsive" src="img/footer/4.jpg" alt="flickr"></a>
                                    </li>
                                    <li>
                                        <a href="#"><img class="img-responsive" src="img/footer/5.jpg" alt="flickr"></a>
                                    </li>
                                    <li>
                                        <a href="#"><img class="img-responsive" src="img/footer/6.jpg" alt="flickr"></a>
                                    </li>
                                </ul>
                            </div>
                        </div> -->
                    </div>
                </div>
            </div>
            <div class="footer-area-bottom">
                <div class="container text-white">
                    &copy; {{ date('Y') }} {{ $site->title }}
                    <!-- <div class="row">

                        <div class="col-xl-12 col-lg-12 col-md-4 col-sm-12 justify-content-end">
                            <ul class="payment-method">
                                <li>
                                    <a href="#"><img alt="payment-method" src="img/payment-method1.jpg"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="payment-method" src="img/payment-method2.jpg"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="payment-method" src="img/payment-method3.jpg"></a>
                                </li>
                                <li>
                                    <a href="#"><img alt="payment-method" src="img/payment-method4.jpg"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div> -->
            </div>
        </footer>
        <!-- Footer Area End Here -->
    </div>
    <!-- Main Body Area End Here -->
    <!-- jquery-->
    <script src="js/jquery-2.2.4.min.js" type="text/javascript"></script>
    <!-- Plugins js -->
    <script src="js/plugins.js" type="text/javascript"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js" type="text/javascript"></script>
    <!-- WOW JS -->
    <script src="js/wow.min.js"></script>
    <!-- Nivo slider js -->
    <script src="vendor/slider/js/jquery.nivo.slider.js" type="text/javascript"></script>
    <script src="vendor/slider/home.js" type="text/javascript"></script>
    <!-- Owl Cauosel JS -->
    <script src="vendor/OwlCarousel/owl.carousel.min.js" type="text/javascript"></script>
    <!-- Meanmenu Js -->
    <script src="js/jquery.meanmenu.min.js" type="text/javascript"></script>
    <!-- Srollup js -->
    <script src="js/jquery.scrollUp.min.js" type="text/javascript"></script>
    <!-- jquery.counterup js -->
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <!-- Countdown js -->
    <script src="js/jquery.countdown.min.js" type="text/javascript"></script>
    <!-- Isotope js -->
    <script src="js/isotope.pkgd.min.js" type="text/javascript"></script>
    <!-- Magic Popup js -->
    <script src="js/jquery.magnific-popup.min.js" type="text/javascript"></script>
    <!-- Gridrotator js -->
    <script src="js/jquery.gridrotator.js" type="text/javascript"></script>
    <!-- Custom Js -->
    <script src="js/main.js" type="text/javascript"></script>

  

    @yield('extra_scripts')


</body>

</html>