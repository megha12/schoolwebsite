@extends('web.layouts.app')

@section('main_section')

<!-- Inner Page Banner Area Start Here -->
<div class="inner-page-banner-area" style="background-image: url('img/banner/5.jpg');">
    <div class="container">
        <div class="pagination-area">
            <h1>Transfer Certificate</h1>
            <ul>
                <li><a href="{{ url('/') }}">Home</a> -</li>
                <li>Transfer Certificate</li>
            </ul>
        </div>
    </div>
</div>
<!-- Inner Page Banner Area End Here -->
<div class="container rounded bg-white align-items-center my-5">
    <div class="mb-5">
        <div class="d-flex justify-content-between align-items-center mb-3">
            <h4 class="text-right">Search By</h4>
        </div>
        <form action="">
            <div class="row">
                <div class="col-sm-5">
                    <div class="mb-3">
                        {{ Form::label('tc_num', 'TC Number') }}
                        {{ Form::text('tc_num', null, ['class' => 'form-control', 'placeholder' => 'Enter TC Number']) }}
                    </div>
                    <div class="mb-3 text-center">OR</div>
                    <div class="mb-3">
                        {{ Form::label('enroll', 'Enrollment Number') }}
                        {{ Form::text('enroll', null, ['class' => 'form-control', 'placeholder' => 'Enter Enrollment Number']) }}
                    </div>
                    <button class="btn btn-secondary px-5" type="submit">Find</button>
                </div>
            </div>
        </form>
    </div>
    
    @if(!empty($tcs))
    <div>
        <hr>
        <h2>Search Results</h2>
        @if($tcs->isEmpty())
            <p>
                No search results found.
            </p>
        @else
            @foreach($tcs as $tc)
            <div class="tc-box rounded">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h3 class="my-0">{{ $tc->title }}</h3>
                        <div>
                            <strong>TC No.: </strong> {{ $tc->getMetaValue('tc_number') }}
                        </div>
                        <div>
                            <strong>Enroll: </strong> {{ $tc->getMetaValue('enroll_no') }}
                        </div>
                    </div>
                    <div>
                        <a href="{{ url('storage/' . $tc->getMetaValue('pdf_file')) }}" target="_blank" class="btn btn-dark">Download</a>
                    </div>
                </div>
            </div>
            @endforeach
        @endif 
    </div>
    @endif
</div>


@endsection