@extends('web.layouts.app')

@section('main_section')

@if(!$sliders->isEmpty())
<!-- Slider 1 Area Start Here -->
<div class="slider1-area overlay-default">
    <div class="bend niceties preview-1">
        <div id="ensign-nivoslider-3" class="slides">
            @foreach($sliders as $s)
            <img src="{{ url('storage/' . $s->image) }}" alt="{{ $s->title }}" title="#slider-direction-1" />
            @endforeach
            <!-- <img src="img/slider/2-2.jpg" alt="slider" title="#slider-direction-2" />
            <img src="img/slider/1-2.jpg" alt="slider" title="#slider-direction-3" /> -->
        </div>
        <!-- <div id="slider-direction-1" class="t-cn slider-direction">
            <div class="slider-content s-tb slide-1">
                <div class="title-container s-tb-c">
                    <div class="title1">Best Education For UI Design</div>
                    <p>Emply dummy text of the printing and typesetting industry orem Ipsum has been the industry's
                        standard
                        <br>dummy text ever sinceprinting and typesetting industry.
                    </p>

                </div>
            </div>
        </div>
        <div id="slider-direction-2" class="t-cn slider-direction">
            <div class="slider-content s-tb slide-2">
                <div class="title-container s-tb-c">
                    <div class="title1">Best Education For HTML Template</div>
                    <p>Emply dummy text of the printing and typesetting industry orem Ipsum has been the industry's
                        standard
                        <br>dummy text ever sinceprinting and typesetting industry.
                    </p>

                </div>
            </div>
        </div>
        <div id="slider-direction-3" class="t-cn slider-direction">
            <div class="slider-content s-tb slide-3">
                <div class="title-container s-tb-c">
                    <div class="title1">Best Education Into PHP</div>
                    <p>Emply dummy text of the printing and typesetting industry orem Ipsum has been the industry's
                        standard
                        <br>dummy text ever sinceprinting and typesetting industry.
                    </p>

                </div>
            </div>
        </div> -->
    </div>
</div>
<!-- Slider 1 Area End Here -->
@endif

<!-- About 2 Area Start Here -->
<div class="about2-area">
    <div class="container">
        <h1 class="about-title">Welcome To {{ $site->title }}</h1>
        <p class="about-sub-title">{{ $about->exceprt }}</p>
    </div>
    <div class="container">
        <div class="row">
            <div class="col lg-8">
                <div class="paragraph">
                    <h1>{{ $about->title }}</h1>
                    {!! $about->description !!}
                </div>
            </div>
            <div class="col-lg-4">
                <div class="cardd">
                    <div class="card border-success mb-3">
                        <div class="card-header  border-success">Notice Board</div>
                        <div class="card-body bodyvcf text-success">
                        <marquee  class="maq" onmouseover="stop()" onmouseout="start()" behavior="scroll" direction="up">
                            @if(!$notices->isEmpty())
                            <ol>
                                @foreach($notices as $n)
                                <li>
                                    <a href="{{ route('post.show', $n->slug) }}">{{ $n->title }}</a>
                                </li>
                                @endforeach
                            </ol>
                            @endif
                        </marquee>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>



</div>

</div>
</div>
<!-- Courses 1 Area Start Here -->
<div class="courses1-area">
    <div class="container">
        <h2 class="title-default-left">Upcoming Events</h2>
    </div>
    <div id="shadow-carousel" class="container">
        <div class="rc-carousel" data-loop="true" data-items="4" data-margin="20" data-autoplay="false"
            data-autoplay-timeout="10000" data-smart-speed="2000" data-dots="false" data-nav="true"
            data-nav-speed="false" data-r-x-small="1" data-r-x-small-nav="true" data-r-x-small-dots="false"
            data-r-x-medium="2" data-r-x-medium-nav="true" data-r-x-medium-dots="false" data-r-small="2"
            data-r-small-nav="true" data-r-small-dots="false" data-r-medium="3" data-r-medium-nav="true"
            data-r-medium-dots="false" data-r-large="4" data-r-large-nav="true" data-r-large-dots="false">
            @foreach ($events as $e)
                <div class="courses-box1">
                    <div class="single-item-wrapper">
                        <div class="courses-img-wrapper hvr-bounce-to-bottom">
                            <img class="img-responsive" src="{{ url('storage/' . $e->image ) }}" alt="courses">
                            <a href="{{ route('post.show', $e->slug) }}"><i class="fa fa-link" aria-hidden="true"></i></a>
                        </div>
                        <div class="courses-content-wrapper">
                            <h3 class="item-title"><a href="{{ route('post.show', $e->slug) }}">{{ $e->title }}</a></h3>
                            <p class="item-content">{{ $e->excerpt }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<!-- Courses 1 Area End Here -->

<!-- Lecturers Area Start Here -->
<div class="lecturers-area">
    <div class="container">
        <h2 class="title-default-left">Our Management</h2>
    </div>
    <div class="container">
        <div class="rc-carousel" data-loop="true" data-items="4" data-margin="30" data-autoplay="true"
            data-autoplay-timeout="10000" data-smart-speed="2000" data-dots="false" data-nav="true"
            data-nav-speed="false" data-r-x-small="1" data-r-x-small-nav="true" data-r-x-small-dots="false"
            data-r-x-medium="2" data-r-x-medium-nav="true" data-r-x-medium-dots="false" data-r-small="3"
            data-r-small-nav="true" data-r-small-dots="false" data-r-medium="4" data-r-medium-nav="true"
            data-r-medium-dots="false" data-r-large="4" data-r-large-nav="true" data-r-large-dots="false">
            @foreach ($management as $m)
                <div class="single-item">
                    <div class="lecturers1-item-wrapper">
                        <div class="lecturers-img-wrapper">
                            <a href="{{ route('post.show', $m->slug) }}"><img class="img-responsive" src="{{ url('storage/' . $m->image) }}" alt="team"></a>
                        </div>
                        <div class="lecturers-content-wrapper">
                            <h3 class="item-title"><a href="{{ route('post.show', $m->slug) }}">{{ $m->title }}</a></h3>
                            <span class="item-designation">{{ $m->excerpt }}</span>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
<!-- Lecturers Area End Here -->
<!-- Video Area Start Here -->
<!-- <div class="video-area overlay-video bg-common-style" style="background-image: url('img/banner/1.jpg');">
    <div class="container">
        <div class="video-content">
            <h2 class="video-title">Watch Campus Life Video Tour</h2>
            <p class="video-sub-title">Vmply dummy text of the printing and typesetting industryorem
                <br>Ipsum industry's standard dum an unknowramble.
            </p>
            <a class="play-btn popup-youtube wow bounceInUp" data-wow-duration="2s" data-wow-delay=".1s"
                href="http://www.youtube.com/watch?v=1iIZeIy7TqM"><i class="fa fa-play" aria-hidden="true"></i></a>
        </div>
    </div>
</div> -->
<!-- Video Area End Here -->
<!-- News and Event Area Start Here -->
<div class="news-event-area">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 news-inner-area">
                <h2 class="title-default-left">Latest News</h2>
                <ul class="news-wrapper">
                    @foreach($news as $n)
                    <li>
                        <div class="news-img-holder">
                            <a href="{{ route('post.show', $n->slug) }}"><img src="{{ url('storage/' . $n->image) }}" class="img-responsive" alt="news"></a>
                        </div>
                        <div class="news-content-holder">
                            <h3><a href="{{ route('post.show', $n->slug) }}">{{ $n->title }}</a></h3>
                            <div class="post-date">{{ date('F d, Y', strtotime($n->created_at)) }}</div>
                            <p>{{ $n->excerpt }}</p>
                        </div>
                    </li>
                    @endforeach
                </ul>
                <!-- <div class="news-btn-holder">
                    <a href="#" class="view-all-accent-btn">View All</a>
                </div> -->
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 event-inner-area">
                <h2 class="title-default-left">Latest Blogs</h2>
                <ul class="event-wrapper">
                    @foreach($blogs as $b)
                    <li class="wow bounceInUp" data-wow-duration="2s" data-wow-delay=".1s">
                        <div class="event-calender-wrapper">
                            <div class="event-calender-holder">
                                <h3>{{ date('d', strtotime($b->created_at)) }}</h3>
                                <p>{{ date('M', strtotime($b->created_at)) }}</p>
                                <span>{{ date('Y', strtotime($b->created_at)) }}</span>
                            </div>
                        </div>
                        <div class="event-content-holder">
                            <h3><a href="{{ route('post.show', $b->title) }}">{{ $b->title }}</a></h3>
                            <p>{{ $b->excerpt }}</p>
                        </div>
                    </li>
                    @endforeach
                </ul>
                <!-- <div class="event-btn-holder">
                    <a href="#" class="view-all-primary-btn">View All</a>
                </div> -->
            </div>
        </div>
    </div>
</div>
<!-- News and Event Area End Here -->
<!-- Students Say Area Start Here -->
<div class="students-say-area">
    <h2 class="title-default-center">Parents Testimonials</h2>
    <div class="container">
        <div class="rc-carousel" data-loop="true" data-items="2" data-margin="30" data-autoplay="false"
            data-autoplay-timeout="10000" data-smart-speed="2000" data-dots="true" data-nav="false"
            data-nav-speed="false" data-r-x-small="1" data-r-x-small-nav="false" data-r-x-small-dots="true"
            data-r-x-medium="2" data-r-x-medium-nav="false" data-r-x-medium-dots="true" data-r-small="2"
            data-r-small-nav="false" data-r-small-dots="true" data-r-medium="2" data-r-medium-nav="false"
            data-r-medium-dots="true" data-r-large="2" data-r-large-nav="false" data-r-large-dots="true">
            @foreach ($testimonials as $t)
                <div class="single-item">
                    <div class="single-item-wrapper">
                        <div class="profile-img-wrapper">
                            <a href="{{ route('post.show', $t->slug) }}" class="profile-img"><img class="profile-img-responsive img-circle" src="{{ url('storage/' . $t->image) }}" alt="{{ $t->title }}"></a>
                        </div>
                        <div class="tlp-tm-content-wrapper">
                            <h3 class="item-title"><a href="{{ route('post.show', $t->slug) }}">{{ $t->title }}</a></h3>
                            
                            <div class="can">
                                <div class="item-content">{{ $t->excerpt }}</div>
                            </div>      
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

<div class="news-event-area">
    <div class="container">
        <h2 class="title-default-center">Gallery</h2>
        @if(!$galleries->isEmpty())
            <div class="row featuredContainer gallery-wrapper">
                @foreach($galleries as $g)
                <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 library auditoriam">
                    <div class="gallery-box">
                        <img src="{{ url('storage/' . $g->image) }}" class="img-responsive" alt="gallery">
                        <div class="gallery-content">
                            <a href="{{ url('storage/' . $g->image) }}" class="zoom"><i class="fa fa-link" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        @endif
    </div>
</div>
<!-- Students Join 2 Area Start Here -->
<!-- <div class="students-join2-area">
    <div class="container">
        <div class="students-join2-wrapper">
            <div class="students-join2-left">
                <div id="ri-grid" class="author-banner-bg ri-grid header text-center">
                    <ul class="ri-grid-list">
                        <li>
                            <a href="#"><img src="img/students/student1.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student2.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student3.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student4.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student5.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student6.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student7.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student8.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student1.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student2.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student3.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student4.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student5.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student6.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student7.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student8.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student1.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student2.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student3.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student4.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student5.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student6.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student7.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student8.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student1.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student2.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student3.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student4.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student5.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student6.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student7.jpg" alt=""></a>
                        </li>
                        <li>
                            <a href="#"><img src="img/students/student8.jpg" alt=""></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="students-join2-right">
                <div>
                    <h2>Gallery</h2>

                </div>
            </div>
        </div>
    </div>
</div> -->
<!-- Students Join 2 Area End Here -->
<!-- Brand Area Start Here -->
<div class="brand-area">
    <div class="container">
        <div class="rc-carousel" data-loop="true" data-items="4" data-margin="30" data-autoplay="true"
            data-autoplay-timeout="5000" data-smart-speed="2000" data-dots="false" data-nav="false"
            data-nav-speed="false" data-r-x-small="2" data-r-x-small-nav="false" data-r-x-small-dots="false"
            data-r-x-medium="3" data-r-x-medium-nav="false" data-r-x-medium-dots="false" data-r-small="4"
            data-r-small-nav="false" data-r-small-dots="false" data-r-medium="4" data-r-medium-nav="false"
            data-r-medium-dots="false" data-r-large="4" data-r-large-nav="false" data-r-large-dots="false">

            @foreach ($brands as $b)
                <div class="brand-area-box">
                    <a href="#"><img src="{{ url('storage/' . $b->slug) }}" alt="{{ $b->title }}"></a>
                </div>
            @endforeach
        </div>
    </div>
</div>
<!-- Brand Area End Here -->



@endsection