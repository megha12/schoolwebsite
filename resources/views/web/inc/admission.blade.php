@extends('web.layouts.app')
@section('main_section')

       <div class="inner-page-banner-area" style="background-image: url('img/banner/5.jpg');">
            <div class="container">
                <div class="pagination-area">
                    <h1>Registration</h1>
                    <ul>
                        <li><a href="{{ url('/') }}">Home</a> -</li>
                        <li>Registration</li>
                    </ul>
                </div>
            </div>
        </div>

<!-- <form name="form" method="post" action="registration_submit"> -->
{{ Form::open(['url' => route('admission.send', ['id' => 'contact-form'])]) }}

    <div class="container py-5">
        <div class="row mb-3 align-items-center">
            <div class="col-sm-3 offset-sm-2">
                {{ Form::label('year', 'Admission For Academic Year') }}
            </div>
            <div class="col-sm-5">
                {{ Form::select('year', ['2022-23' => '2022-23'], null, ['class' => 'form-select', 'placeholder' => 'Select Year', 'required' => 'required']) }}
            </div>
        </div>
        <div class="row mb-3 align-items-center">
            <div class="col-sm-3 offset-sm-2">
                {{ Form::label('class', 'Admission Sought to') }}
            </div>
            <div class="col-sm-5">
                {{ Form::select('class', [
                    'Pre Nursery' => 'Pre Nursery',
                    'Nursery' => 'Nursery',
                    'LKG' => 'LKG',
                    'UKG' => 'UKG',
                    'I' => 'I',
                    'II' => 'II',
                    'III' => 'III',
                    'IV' => 'IV',
                    'V' => 'V',
                    'VI' => 'VI',
                    'VII' => 'VII',
                    'VIII' => 'VIII',
                    'IX' => 'IX',
                    'X' => 'X'
                ], null, ['class' => 'form-select', 'placeholder' => 'Select Class', 'required' => 'required']) }}
            </div>
        </div>
        <div class="row mb-3 align-items-center">
            <div class="col-sm-3 offset-sm-2">
                {{ Form::label('name', 'Student\'s Name') }}
            </div>
            <div class="col-sm-5">
                {{ Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Student\'s Name', 'required' => 'required']) }}
            </div>
        </div>
        <div class="row mb-3 align-items-center">
            <div class="col-sm-3 offset-sm-2">
                {{ Form::label('father_name', 'Father\'s Name') }}
            </div>
            <div class="col-sm-5">
                {{ Form::text('father_name', null, ['class' => 'form-control', 'placeholder' => 'Father\'s Name', 'required' => 'required']) }}
            </div>
        </div>
        <div class="row mb-3 align-items-center">
            <div class="col-sm-3 offset-sm-2">
                {{ Form::label('father_occupation', 'Occupation') }}
            </div>
            <div class="col-sm-5">
                {{ Form::text('father_occupation', null, ['class' => 'form-control', 'placeholder' => 'Occupation','required' => 'required']) }}
            </div>
        </div>
        <div class="row mb-3 align-items-center">
            <div class="col-sm-3 offset-sm-2">
                {{ Form::label('father_phone', 'Father\'s Mobile No.') }}
            </div>
            <div class="col-sm-5">
                {{ Form::tel('father_phone', null, ['class' => 'form-control', 'placeholder' => 'Father\'s Mobile No.', 'required' => 'required']) }}
            </div>
        </div>
        <div class="row mb-3 align-items-center">
            <div class="col-sm-3 offset-sm-2">
                {{ Form::label('father_email', 'Email Address') }}
            </div>
            <div class="col-sm-5">
                {{ Form::email('father_email', null, ['class' => 'form-control', 'placeholder' => 'Email Address','required' => 'required']) }}
            </div>
        </div>

        <div class="row mb-3 align-items-center">
            <div class="col-sm-3 offset-sm-2">
                {{ Form::label('mother_name', 'Mother\'s Name') }}
            </div>
            <div class="col-sm-5">
                {{ Form::text('mother_name', null, ['class' => 'form-control', 'placeholder' => 'Mother\'s Name', 'required' => 'required']) }}
            </div>
        </div>
        <div class="row mb-3 align-items-center">
            <div class="col-sm-3 offset-sm-2">
                {{ Form::label('mother_occupation', 'Occupation') }}
            </div>
            <div class="col-sm-5">
                {{ Form::text('mother_occupation', null, ['class' => 'form-control', 'placeholder' => 'Occupation']) }}
            </div>
        </div>
        <div class="row mb-3 align-items-center">
            <div class="col-sm-3 offset-sm-2">
                {{ Form::label('mother_phone', 'Mother\'s Mobile No.') }}
            </div>
            <div class="col-sm-5">
                {{ Form::tel('mother_phone', null, ['class' => 'form-control', 'placeholder' => 'Mother\'s Mobile No.', 'required' => 'required']) }}
            </div>
        </div>
        <div class="row mb-3 align-items-center">
            <div class="col-sm-3 offset-sm-2">
                {{ Form::label('mother_email', 'Email Address') }}
            </div>
            <div class="col-sm-5">
                {{ Form::email('mother_email', null, ['class' => 'form-control', 'placeholder' => 'Email Address']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-8 offset-sm-2">
                <p>Please recheck mobile number before submitting the form as you will receive a message carrying registration number on the same.</p>
            </div>
        </div>
        <div class="text-center">
            <button class="default-big-btn">
                Submit
            </button>
        </div>
    </div>
</form>

@endsection

@section('extra_scripts')

{!! RecaptchaV3::initJs() !!}

@endsection