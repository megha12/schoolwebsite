@extends('web.layouts.app')

@section('main_section')
<!-- Inner Page Banner Area Start Here -->
<div class="inner-page-banner-area" style="background-image: url('img/banner/5.jpg');">
    <div class="container">
        <div class="pagination-area">
            <h1>Contact Us</h1>
            <ul>
                <li><a href="{{ url('/') }}">Home</a> -</li>
                <li>Contact</li>
            </ul>
        </div>
    </div>
</div>
<!-- Inner Page Banner Area End Here -->
<!-- Contact Us Page 2 Area Start Here -->
<div class="contact-us-page2-area">
    <div class="container">
        @if(\Session::has('success'))
        <p class="alert alert-success">{{ \Session::get('success') }}</p>
        @endif
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12">
                <h2 class="title-default-left title-bar-high">Information</h2>
                <div class="contact-us-info2">
                    <ul>
                        <li><i class="fa fa-map-marker" aria-hidden="true"></i>{{ nl2br($site->address) }}</li>
                        <li><i class="fa fa-phone" aria-hidden="true"></i><a href="tel:{{ $site->phone }}">{{ $site->phone }} </a></li>
                        <li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto: {{ $site->email }}">{{ $site->email }}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12">
                <div class="row">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                        <h2 class="title-default-left title-bar-high">Contact With Us</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="contact-form2">
                        <!-- <form id="contact-form"> -->
                        {{ Form::open(['url' => route('contact.send', ['id' => 'contact-form'])]) }}
                            <fieldset>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Name</label>
                                        <input type="text" placeholder="Name*" class="form-control" name="name" id="form-name" data-error="Name field is required" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="email" placeholder="Email*" class="form-control" name="email" id="form-email" data-error="Email field is required" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>How did you know about us ?</label>
                                        {{ Form::select('how_know', ['facebook' => 'Facebook', 'instagram' => 'Instagram','twitter' => 'Twitter','news paper' => 'News Paper'], null, ['class' => 'form-select', 'required' => 'required', 'placeholder' => 'Select Any Option']) }}
                                        <!-- <input type="text"  class="form-select" name="social" id="social" data-error="Email field is required" required> -->
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Message</label>
                                        <textarea placeholder="Message*" class="textarea form-control" name="message" id="form-message" rows="8" cols="20" data-error="Message field is required" required></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-4 col-md-6 col-md-12">
                                    <div class="form-group margin-bottom-none">
                                        <button type="submit" class="default-big-btn">Send Message</button>
                                    </div>
                                </div>
                                <div class="col-xl-8 col-lg-8 col-md-6 col-md-12">
                                    <div class='form-response'></div>
                                </div>
                            </fieldset>
                        <!-- </form> -->
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="container-fluid">
        <div class="row">
            <div class="google-map-area">
                <div id="googleMap" style="width:100%; height:395px;"></div>
            </div>
        </div>
    </div> -->
</div>
<!-- Contact Us Page 2 Area End Here -->
@endsection
@section('extra_scripts')

{!! RecaptchaV3::initJs() !!}

@endsection