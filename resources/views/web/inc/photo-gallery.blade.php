@extends('web.layouts.app')

@section('main_section')
       <!-- Inner Page Banner Area Start Here -->
       <div class="inner-page-banner-area" style="background-image: url('img/banner/5.jpg');">
            <div class="container">
                <div class="pagination-area">
                    <h1>Gallery</h1>
                    <ul>
                        <li><a href="{{ url('/') }}">Home</a> -</li>
                        <li>Gallery</li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Inner Page Banner Area End Here -->
        <!-- Gallery Area 2 Start Here -->
        <div class="gallery-area2">
            <div class="container" id="inner-isotope">
                @if(!$galleries->isEmpty())
                <div class="row featuredContainer gallery-wrapper">
                    @foreach($galleries as $g)
                    <div class="col-xl-3 col-lg-3 col-md-4 col-sm-12 library auditoriam">
                        <div class="gallery-box">
                            <img src="{{ url('storage/' . $g->image) }}" class="img-responsive" alt="gallery">
                            <div class="gallery-content">
                                <a href="{{ url('storage/' . $g->image) }}" class="zoom"><i class="fa fa-link" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                {{ $galleries->links() }}
                @else
                <p>No gallery images found.</p>
                @endif
            </div>
        </div>
        <!-- Gallery Area 2 End Here -->
@endsection