@extends('web.layouts.app')

@section('main_section')
<!-- Inner Page Banner Area Start Here -->
<div class="inner-page-banner-area" style="background-image: url({{ !empty($post->image) ? url('storage/' . $post->image) : url('img/banner/5.jpg') }});">
    <div class="container">
        <div class="pagination-area">
            <h1>{{ $post->title }}</h1>
            
            <ul>
                <li><a href="{{ url('/') }}">Home</a> -</li>
                <li>{{ $post->title }}</li>
            </ul>
        </div>
    </div>
</div>
<!-- Inner Page Banner Area End Here -->
<!-- Contact Us Page 2 Area Start Here -->
<div class="contact-us-page2-area">
    <div class="container">
        <p>
            {!! $post->description !!}
        </p>
    </div>
</div>
@endsection