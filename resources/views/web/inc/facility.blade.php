@extends('web.layouts.app')

@section('main_section')


    <!-- Inner Page Banner Area Start Here -->
    <div class="inner-page-banner-area" style="background-image: url('img/banner/5.jpg');">
        <div class="container">
            <div class="pagination-area">
                <h1>{{ $post->title }}</h1>
                <ul>
                    <li><a href="{{ url('/') }}">Home</a> -</li>
                    <li>{{ $post->title }}</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Inner Page Banner Area End Here -->
    <!-- Research Page 1 Area Start Here -->
    <div class="research-page1-area">
        <div class="container">
            @if(!$facilities->isEmpty())
            <div class="row">
                @foreach($facilities as $d)
                <div class="col-md-4">
                    <div class="research-box1">
                        <img src="{{ url('storage/' . $d->image) }}" class="img-responsive" alt="research">
                        <h3 class="sidebar-title"><a href="{{ route('post.show', $d->slug) }}">{{ $d->title }}</a></h3>
                    </div>
                </div>
                @endforeach
            </div>
            {{ $downloads->links() }}
            @else
            <p>No downloads content found</p>
            @endif
        </div>
    </div>
    <!-- Research Page 1 Area End Here -->
@endsection