<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_fields', function (Blueprint $table) {
            $table->id();
            $table->string('post_type')->default('post');
            $table->string('title')->default('Title')->nullable();
            $table->string('excerpt')->default('Short Description')->nullable();
            $table->string('description')->default('Description')->nullable();
            $table->string('image')->default('Featured Image')->nullable();
            $table->string('meta_title')->default('Meta Title')->nullable();
            $table->string('meta_keywords')->default('Meta Keywords')->nullable();
            $table->string('meta_description')->default('Meta Description')->nullable();
            $table->string('template')->default('Template')->nullable();
            $table->json('extra_fields')->nullable();
            $table->unsignedBigInteger('site_id');
            $table->foreign('site_id')->references('id')->on('sites')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_fields');
    }
};