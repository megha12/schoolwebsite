<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sites', function (Blueprint $table) {
            $table->json('social_links')->after('address')->nullable()->comment('json');
            $table->text('google_map')->after('social_links')->nullable();
            $table->string('primary_color')->after('google_map')->nullable();
            $table->string('secondary_color')->after('primary_color')->nullable();
            $table->unsignedBigInteger('post_id')->after('secondary_color')->comment('for homepage')->nullable();
            $table->foreign('post_id')->references('id')->on('posts')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sites', function (Blueprint $table) {
            //
        });
    }
};