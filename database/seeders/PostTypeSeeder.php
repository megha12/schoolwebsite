<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\PostType;

class PostTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PostType::insert([
            'name'  => 'Post',
            'slug'  => 'post',
            'site_id' => 1,
        ]);
        PostType::insert([
            'name'  => 'Page',
            'slug'  => 'page',
            'site_id' => 1,
        ]);
        
        PostType::insert([
            'name'  => 'Slider',
            'slug'  => 'slider',
            'site_id' => 1,
        ]);
        PostType::insert([
            'name'  => 'Testimonial',
            'slug'  => 'testimonial',
            'site_id' => 1,
        ]);
        PostType::insert([
            'name'  => 'Download',
            'slug'  => 'Download',
            'site_id' => 1,
        ]);
        PostType::insert([
            'name'  => 'TC',
            'slug'  => 'TC',
            'site_id' => 1,
        ]);
        PostType::insert([
            'name'  => 'Photo-gallary',
            'slug'  => 'Photo-gallary',
            'site_id' => 1,
        ]);
        PostType::insert([
            'name'  => 'Video-gallary',
            'slug'  => 'Video-gallary',
            'site_id' => 1,
        ]);
    }
}
